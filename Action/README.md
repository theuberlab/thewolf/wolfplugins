# Action Plugins for [TheWolf](https://bitbucket.org/theuberlab/thewolf)

Action Plugins are the plugins which actually DO something. 
Run a command, Delete an EC2 instance, run a re-indexing job etc.