# TEXTResponder Plugin for [TheWolf](https://bitbucket.org/theuberlab/thewolf)

The TEXTResponder plugin is the "Hello World" plugin for TheWolf.

It takes text as a config object and when called simply returns it.
It will at some point allow for templating but Rome wasn't built in a day. ^_^