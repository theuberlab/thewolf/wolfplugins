package main

import (
	"bytes"
	"fmt"
	"github.com/go-validator/validator"
	"os/exec"
	"text/template"

	"bitbucket.org/theuberlab/lug"
	"bitbucket.org/theuberlab/thewolf/plugins"
)

// Not actually a string, just a handle for all our methods.
type plugin string

// Expose ourselves as a public variable so that all our methods can be fetched.
var WolfPlugin plugin

var (
	name		= "ExternalCommandExecutor"
	version 	= "0.0.1"
	pluginType 	= plugins.PLUGIN_TYPE_ACTION
)

//TODO: Implement this. There will definately be some default options we want to set I just can't think of them. path? command timeout. etc.
// The plugin configuration
//type externalCommandExecutorConfig struct {
//	Path 			string
//	Shell 			string
//}

// The format for the configuration portion of an Action
type externalCommandExecutorActionConfig struct {
	Command			string 			`yaml:"Command" validate:"nonzero"` // The text to return when called
	Args	 		[]string	 	`yaml:"Args,omitempty"` // Human readable comments.
}

// What to do if the initial action fails.
func (p *plugin) failureTrigger(interface{}) {

}

// Actually validate the config file section
func (p *plugin) validateActionConfig(config interface{}) error {
	return validator.Validate(config.(externalCommandExecutorActionConfig))
}

// Perform the main activity when the plugin is called.
// This wayis stupid but it's v0.0.1 ultimately we should probably have a GetTriggerFunction() Which returns a reference to a function like GetConfigValidator() and GetFailureFunction()
func (p *plugin) TriggerAction(instance plugins.ActionInstance) string {
	lug.Debug("Message", "TriggerAction called.")

	//responderConfig := config.Config.(hTMLResponderConfig)

	lug.Debug("Message", "Recieved ActionInstance", "ActionInstance", fmt.Sprintf("%v", instance))
	executorConfig := instance.Config.Config.(map[string]interface{})

	var tpl bytes.Buffer

	cmdName := executorConfig["Command"].(string)

	cmdArgsArray := executorConfig["Args"].([]interface{})


	// error: iArr.([]string)
	//for _, v := range iArr {
	//	fmt.Printf(v.(string) + "\n")
	//}
	var cmdArgs []string

	// Check for templatized variables first?
	for _, arg := range cmdArgsArray {

		t, err := template.New("template").Parse(arg.(string))

		lug.Error("Message", "Failed to parse template", "Error", err)

		err = t.ExecuteTemplate(&tpl, "template", instance.Job)

		lug.Error("Message", "Failed to execute template", "Error", err)

		cmdArgs = append(cmdArgs, tpl.String())
	}

	cmd := exec.Command(cmdName, cmdArgs...)

	out, err := cmd.CombinedOutput()
	if err != nil {
		lug.Error("Message", "cmd.Run() failed with error", "Error", err)
	}
	lug.Debug("Message", "Combined command output", "Output", out)


	result := fmt.Sprintf("%s", out)

	//TODO: This is some sketchy sketchy garbage right here.
	return result
}

// Get the function which will be used to validat the 'Config' of ActionConfig elements which reference this plugin.
func (p *plugin) GetActionConfigValidator() func(interface{}) error {
	return p.validateActionConfig
}

// return a reference to the configured onFailure function so that thewolf can trigger that.
func (p *plugin) GetFailureAction() func(interface{}) {
	return p.failureTrigger
}

// Return the plugin's name
func (p *plugin) GetPluginName() string {
	return name
}

// Returns a version in symantic format.
func (p *plugin) GetPluginVersion() string {
	return version
}

func (p *plugin) GetPluginType() plugins.PluginType {
	return pluginType
}

// Actually validate the config file section
func (p *plugin) ValidatePluginConfig(config interface{}) error {
	return nil // for now
	//return validator.Validate(config.(externalCommandExecutorConfig))
}